<?php
/**
* Räkna antal ord i inskickad text
*
* PHP version 5
* @category   Räkna ord i text
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/

include "funktioner.php";
?>

<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Räkna ord i en text</title>
    <link rel="stylesheet" href="">
</head>
<body>
    <h1>Räkna ord i texten</h1>
    <?php

    // Om post-variabeln "text" finns då läser vi av den
    if (isset($_POST["text"])) {
        $text = $_POST["text"];
        $antalOrd = str_word_count($text);
        rubrik($antalOrd);
    }
    ?>
    <form method="post">
        <textarea name="text"></textarea>
        <input type="submit" name="Räkna">
    </form>
</body>
</html>
