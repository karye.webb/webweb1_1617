<?php
/**
* En webbapplikation som testar styrkan på ett lösenord. Se http://www.passwordmeter.com/ för tips
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>...</title>
</head>
<body>
    <?php
    if (isset($_POST["password"]) ) {

        $password = $_POST["password"];

        // Antal tecken (totalt)
        $antalTecken = strlen($password);
        // Dela upp lösenordet i en array
        $teckenArray = str_split($password);

        // Antal versaler
        $antalVersaler = 0;
        foreach ($teckenArray as $tecken) {
            if (ctype_upper($tecken))
                $antalVersaler++;
        }

        // Antal gemena
        $antalGemena = 0;
        foreach ($teckenArray as $tecken) {
            if (ctype_lower($tecken))
                $antalGemena++;
        }

        // Antal siffror
        $antalSiffror = 0;
        foreach ($teckenArray as $tecken) {
            if (ctype_digit($tecken))
                $antalSiffror++;
        }

        // Antal symboler
        $antalSymboler = 0;
        foreach ($teckenArray as $tecken) {
            if (ctype_graph($tecken))
                $antalSymboler++;
        }

        // Räkna styrka på lösenordet
        $poang = $antalTecken*4 + ($antalTecken-$antalVersaler)*2 + ($antalTecken-$antalGemena)*2 + $antalSiffror*4 + $antalSymboler*6;

        echo "<p>Styrka på lösenordet är $poang</p>";
    }
    ?>
    <form method="post">
        <h2>Räkna ut styrka på lösenordet</h2>
        <label>Password: </label><input type="text" name="password"><br>
        <input type="submit">
    </form>
</body>
</html>
