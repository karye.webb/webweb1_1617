<?php
/**
* Hämtar dagens horoskop från metro.se
*
* PHP version 5
* @category   Enkel webscrapping
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Mitt horoskop</title>
</head>
<body>

<?php
// Kontrollerar att vi har formulärdata med funktionen isset
if (??? && ???) {

    // Plockar ut formulärdata
    $url = ???;
    $sign = ???;

    // Ladda hem hela webbsidan från metro med funktionen file_get_contents
    $sida = ???;

    // Leta rätt på var horoskoptexten finns med funktionen strpos
    $hittat = ???;

    if ($hittat != false) {
        echo "<p>Horoskopet finns på position $hittat</p>";

        // Letat rätt på slutet på texten med funktionen strpos
        $slut = ???(???, "</p>", ???);

        // Plocka ut horoskoptexten från start till slut med funktionen substr
        $horoskopet = ???;
        echo "<p>$horoskopet</p>";
    }
}
?>
</body>
</html>
