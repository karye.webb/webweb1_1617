<?php
/**
* Skapa batch-fil för skapa användare i webmin
*
* PHP version 5
* @category   Skapa användarlista
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

    <!DOCTYPE html>
    <html lang="sv">

    <head>
        <meta charset="utf-8">
        <title>Skapa användarlista</title>
        <link rel="stylesheet" href="">
        <style>
            textarea {
                width: 500px;
                height: 400px;
            }

        </style>
    </head>

    <body>
    <?php
    if (isset($_POST["lista"])) {
        $emails = explode(',', $_POST["lista"]);
        foreach ($emails as $email) {
            list($username, $domain) = explode('@',$email . "@");
            echo "<p>$username</p>";
        }
    }
    ?>
            <form action="" method="post">
                <textarea name="lista"></textarea>
                <br>
                <input type="submit">
            </form>
    </body>

    </html>
