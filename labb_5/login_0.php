<?php
/**
* Loginsida som kollar upp användare i en textfil
*
* PHP version 5
* @category   Enkel skriptsida
* @author     Karim Ryde <karye.webb@gmail.com>
* @license    PHP CC
* @link
*/
?>

<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Logga in</title>
</head>

<body>
    <?php
    $inloggad = false;

    // Kontrollerar att vi har formulärdata med funktionen isset
    if (??? && ???) {

        // Plocka ut formulärdata
        $user = ???;
        $pass = ???;

        // Rensa bort mellanslag i början och slutet
        $user = ???;
        $pass = ???;

        // Omvandla till små bokstäver
        $user = ???;
        $pass = ???;

        // Läs in hela filen i en array med funktionen file
        $rader = ???("C:/xampp/htdocs/users.txt");

        // Loopa igenom arrayen
        ??? (??? as $index=>$rad) {

            echo "<p>rad=$rad index=$index</p>";

            // Rensa bort tomtecken med funktionen trim
            $rad = ???;

            // Kolla om vi hittar "user=$user"
            if (???) {
                echo "<p>Hittat: rad=$rad index=$index</p>";

                // Hämta raden efter med lösenordshashet
                $radEfter = ???;

                // Plocka ut hashet from position 9 med substr
                $hash = ???;

                // Städa bort skräptecken
                $hash = ???;

                // Kolla om lösenordet stämmer med funktionen password_verify
                if (???) {
                    $inloggad = true;
                }
            }
        }
    }

    // Om ej inloggad
    if (!$inloggad) {

        // Om obefintligt formulärdata
        if (??? && ???) {
            echo "<h1>Fel användarnamn eller lösenord! Försök igen.</h1>";
        }
        ?>
        <form method="post">
            <h2>Logga in användare</h2>
            <label>Username: </label><input type="text" name="user"><br>
            <label>Password: </label><input type="password" name="pass"><br>
            <input type="submit">
        </form>
        <?php
    } else {
        echo "<h1>Du är nu inloggad!</h1>";
    }
    ?>
</body>

</html>
