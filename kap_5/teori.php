<?php

$mat[] = "Pizza Carbonara"; // Skapar en ny array som heter $mat
$mat[] = "Kötbullar"; // Fyll på en till position
$mat[] = "Sushi";
$mat[] = "Pannkakor";
$mat[] = "Kebab";
$mat[] = "Lasagne";
$mat[] = "Pizza Calzone";

echo $mat[3]; // Pannkakor

// Lista alla i arrayen
foreach ($mat as $ratt) {
    echo "<p>$ratt</p>";
}

// Baklänges
for ($i = count($mat); $i > 0; $i--) {
    echo "<p>$i: $mat[$i]</p>";
}

// Framlänges
for ($i = 0; $i < count($mat); $i++) {
    echo "<p>$i: $mat[$i]</p>";
}

$ordsprak = "Behandla andra såsom du själv vill bli behandlad.";
$orden = explode(' ', $ordsprak);

$ordsprak2 = "";
foreach ($orden as $ord) {
    $ordsprak2 .= "$ord-";
}
$ordsprak2 = substr($ordsprak2, 0, -1);
echo "<p>$ordsprak2</p>";







